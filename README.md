# ColorClock
A hex clock that converts the current time into a hex color for the page background

Demo here: [Color Clock](https://samuelts.com/ColorClock)
