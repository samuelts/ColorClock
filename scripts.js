setInterval(getDate, 1000);

function getDate() {
  const date = new Date();

  const hours = date.getHours();
  const hourStr = hours < 10 ? `0${hours}` : `${hours}`;

  const minutes = date.getMinutes();
  const minuteStr = minutes < 10 ? `0${minutes}` : `${minutes}`;

  const seconds = date.getSeconds();
  const secondStr = seconds < 10 ? `0${seconds}` : `${seconds}`;

  const timeStr = `${hourStr}${minuteStr}${secondStr}`;
  const L = 0.2126 * (hours/255.0) + 0.7152 * (minutes/255.0) + 0.0722 * (seconds/255.0);
  const textColor = L > 0.179 ? '#222' : '#ddd';
  document.body.style = `color: ${textColor}; background: #${timeStr};`;
  document.getElementById('text').innerHTML = `#${timeStr}`;
}